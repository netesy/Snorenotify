<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>GrowlSettings</name>
    <message>
        <location filename="../src/plugins/backends/growl/growlsettings.cpp" line="30"/>
        <source>Host:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/backends/growl/growlsettings.cpp" line="31"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../src/libsnore/settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.ui" line="34"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.ui" line="45"/>
        <source>Primary Backend:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.ui" line="55"/>
        <source>Timeout:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.ui" line="62"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.ui" line="72"/>
        <source>Primary Backends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.ui" line="82"/>
        <source>Secondary Bckends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.ui" line="92"/>
        <source>Frontends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.ui" line="102"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.ui" line="115"/>
        <source>Display Test Notification</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../src/settings/settingswindow.ui" line="14"/>
        <source>Snore Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/settings/settingswindow.ui" line="23"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SnarlSettings</name>
    <message>
        <location filename="../src/plugins/backends/snarl/snarlsettings.cpp" line="29"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Snore::PluginSettingsWidget</name>
    <message>
        <location filename="../src/libsnore/plugins/pluginsettingswidget.cpp" line="36"/>
        <source>Enabled:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Snore::SettingsDialog</name>
    <message>
        <location filename="../src/libsnore/settingsdialog.cpp" line="79"/>
        <source>Hello World</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.cpp" line="81"/>
        <source>This is Snore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.cpp" line="81"/>
        <source>Project Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/libsnore/settingsdialog.cpp" line="83"/>
        <source>Test Action</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SnoreNotifierSettings</name>
    <message>
        <location filename="../src/plugins/backends/snore/snorenotifiersettings.cpp" line="30"/>
        <source>TopLeftCorner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/backends/snore/snorenotifiersettings.cpp" line="31"/>
        <source>TopRightCorner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/backends/snore/snorenotifiersettings.cpp" line="32"/>
        <source>BottomLeftCorner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/backends/snore/snorenotifiersettings.cpp" line="33"/>
        <source>BottomRightCorner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/backends/snore/snorenotifiersettings.cpp" line="34"/>
        <source>Position:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SoundSettings</name>
    <message>
        <location filename="../src/plugins/secondary_backends/sound/soundsettings.cpp" line="31"/>
        <source>Audio file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/secondary_backends/sound/soundsettings.cpp" line="32"/>
        <source>Select a audio file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/secondary_backends/sound/soundsettings.cpp" line="35"/>
        <source>All Audio files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToastySettings</name>
    <message>
        <location filename="../src/plugins/secondary_backends/toasty/toastysettings.cpp" line="28"/>
        <source>Device ID:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
